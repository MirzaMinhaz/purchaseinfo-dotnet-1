﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PurchaseForm
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string fname = txtFirst_name.Text;
            string mname = txtMiddle_name.Text;
            string lname = txtLast_name.Text;
            string str;
            str = fname+" "+mname+" "+lname;
            //lblOutput.Text = str;
            txtOutput.Text = str;
            string text = str;
            char[] chararray = text.ToCharArray();
            Array.Reverse(chararray);
            string Txt = string.Empty;
            for (int i = 0; i <= chararray.Length - 1; i++)
            {
                Txt += chararray.GetValue(i);
            }
            txtReverse.Text = Txt;
        }
    }
}
