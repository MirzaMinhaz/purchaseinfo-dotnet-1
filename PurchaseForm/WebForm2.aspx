﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="PurchaseForm.WebForm2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label1" runat="server" Text="First Name :"></asp:Label>
                    <asp:TextBox ID="txtFirst_name" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label2" runat="server" Text="Middle Name :"></asp:Label>
                    <asp:TextBox ID="txtMiddle_name" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label3" runat="server" Text="Last Name :"></asp:Label>
                    <asp:TextBox ID="txtLast_name" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button style="margin: 3% 7%; padding: 1% 2%;" ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="lblOutput" runat="server" Text="Full Name :"></asp:Label>
                    <asp:TextBox ID="txtOutput" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="lblReverse" runat="server" Text="Reversed Name :"></asp:Label>
                    <asp:TextBox ID="txtReverse" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
