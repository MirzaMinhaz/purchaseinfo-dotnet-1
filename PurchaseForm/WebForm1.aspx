﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="PurchaseForm.WebForm1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Crystal Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="labelPurchaseId" runat="server" Text="Purchase ID:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropdownID" runat="server" Height="16px" Width="165px">
                        <asp:ListItem Value="">----Select----</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
    </div>
    <div>
        <CR:CrystalReportViewer ID="crvPurchaseInfo" AutoDataBind="true" runat="server" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
        <Report FileName="PurchaseInfoCrystalReport.rpt">
            </Report>
        </CR:CrystalReportSource>
    
       <%-- <CR:CrystalReportViewer ID="crvPurchaseInfo" runat="server" AutoDataBind="true" Height="1055px"
            Width="450px" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="CrystalReport1.rpt">
            </Report>
        </CR:CrystalReportSource>--%>
    </div>
    </form>
</body>
</html>
