﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

namespace PurchaseForm
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        private PurchaseInfoDataSet insertRecord()
        {

            SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=Purchase;Integrated Security=True");


            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
            string conString = string.Format(@"Select PI.PurchaseID,PurchaseBy,Date,CompanyName,PurchaseDetailsID,ItemName,Quantity,UnitPrice,TotalAmount FROM PurchaseInfo PI
            Inner join PurchaseDetailsInfo PD on PD.PurchaseID=PI.PurchaseID
            Where PI.PurchaseID like '" + dropdownID.SelectedValue + "%' order by PurchaseID");

            SqlCommand cmd = new SqlCommand(conString, con);

            SqlDataAdapter sda = new SqlDataAdapter();

            cmd.Connection = con;
            sda.SelectCommand = cmd;
            PurchaseInfoDataSet ds = new PurchaseInfoDataSet();
            sda.Fill(ds, "PurchaseInfo");
            con.Close();
            return ds;
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PurchaseInfoCrystalReport crystalReport = new PurchaseInfoCrystalReport();
            PurchaseInfoDataSet ds = insertRecord();
            crystalReport.SetDataSource(ds);
            this.crvPurchaseInfo.ReportSource = crystalReport;
            this.crvPurchaseInfo.RefreshReport();
            insertRecord();

        }
    }
}
