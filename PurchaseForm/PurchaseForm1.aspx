﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseForm1.aspx.cs" Inherits="PurchaseForm.PurchaseForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Web Form</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox> &nbsp 
        <asp:Button ID="btnSearch"
            runat="server" Text="Search" onclick="btnSearch_Click" />
    </div>
    <div>
    <div>
        <h2>Purchase Information</h2>
        <table>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label1" runat="server" Text="Purchase ID :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtID" runat="server" AutoPostBack="false"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                         ControlToValidate="txtID" ErrorMessage="Enter your Purchase ID " >
                         </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label2" runat="server" Text="Purchase By :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropdownPurchase_by" runat="server">
                        <asp:ListItem Value="0">---Select---</asp:ListItem>
                        <asp:ListItem>Shakib</asp:ListItem>
                        <asp:ListItem>Sadia</asp:ListItem>
                        <asp:ListItem>Nafis</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                         ControlToValidate="dropdownPurchase_by" InitialValue="0" ErrorMessage="Select the value" >
                         </asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label3" runat="server" Text="Date :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDate" runat="server" AutoPostBack="false"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                         ControlToValidate="txtDate" ErrorMessage="Enter Date " >
                         </asp:RequiredFieldValidator> 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label4" runat="server" Text="Company Name :"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dropdownCompany" runat="server">
                        <asp:ListItem Value="0">---Select---</asp:ListItem>
                        <asp:ListItem>Birds A &amp; Z Limited</asp:ListItem>
                        <asp:ListItem>RCL</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                         ControlToValidate="dropdownCompany" InitialValue="0" ErrorMessage="Select the Company Name" >
                         </asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <%--2nd form div--%>
    <div>
        <h2>Purchase Details Information</h2>
        <table>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label5" runat="server" Text="Item Name :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtItem_name" runat="server" AutoPostBack="false"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                         ControlToValidate="txtItem_name" ErrorMessage="Enter Item_name " >
                         </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label6" runat="server" Text="Quantity :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtQuantity" runat="server" AutoPostBack="true" OnTextChanged ="totalCalculation"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                         ControlToValidate="txtQuantity" ErrorMessage="Enter Quantity " >
                         </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label7" runat="server" Text="Unit Price:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUnit_price" runat="server" AutoPostBack="true" OnTextChanged="totalCalculation"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                         ControlToValidate="txtUnit_price" ErrorMessage="Enter Unit Price " >
                         </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label style="display:block; float:left; width:150px;" ID="Label8" runat="server"  Text="Total Amount: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTotal_amount" runat="server" AutoPostBack="false" ReadOnly="true"></asp:TextBox><br />
                   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAddtolist" runat="server" Text="Add to List" OnClick="btnAddtolist_Click" />
                </td>
                 
            </tr>
        </table>
    </div>
    </div>
    <%--gridview div--%>
    <div><br />
        <table>
            <asp:GridView ID="GridView1" runat="server" OnRowEditing="GridView1_RowEditing" 
                AutoGenerateColumns="False">
                <Columns>
                    <%--<asp:BoundField DataField="PurchaseID" HeaderText="Purchase ID" />--%>
                         <asp:BoundField DataField="ItemName" HeaderText="Item Name" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                            <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" />
                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" />
                    
                            <asp:TemplateField>
                                <ItemTemplate>
                                     <asp:LinkButton ID="LinkButton1" Text="Edit" runat="server" CommandName="Edit" /> 
                                    <asp:LinkButton ID="LinkButton2" Text="Delete" runat="server" OnClick="OnDelete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                       <asp:LinkButton ID="LinkButton3" Text="Update" runat="server" OnClick="OnUpdate" /> 
                                    <asp:LinkButton ID="LinkButton4" Text="Cancel" runat="server" OnClick="OnCancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
            </asp:GridView>
        </table>
    </div>
    <div>
    <table>
    <tr>
        <td colspan="2">
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
        </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblSuccessMessage" runat="server" Text="" ForeColor="Green"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
     </table>
    </div>
  
    </form>
</body>
</html>
