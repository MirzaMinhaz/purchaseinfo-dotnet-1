﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace PurchaseForm
{
    public partial class PurchaseForm1 : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=(local);Initial Catalog=Purchase;Integrated Security=True");

        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                dt.Columns.Add("ItemName", Type.GetType("System.String"));
                dt.Columns.Add("Quantity", Type.GetType("System.Decimal"));
                dt.Columns.Add("UnitPrice", Type.GetType("System.Decimal"));
                dt.Columns.Add("TotalAmount", Type.GetType("System.Decimal"));
                ViewState["dt"] = dt;
                //BindGrid();
            }
        }

        protected void BindGrid()
        {
            GridView1.DataSource = ViewState["dt"] as DataTable;
            GridView1.DataBind();
        }

        // rowEditing part

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindGrid();
        }

        // update part

        protected void OnUpdate(object sender, EventArgs e)
        {
            GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
            string itemname = ((TextBox)row.Cells[0].Controls[0]).Text;
            decimal quantity = Convert.ToDecimal(((TextBox)row.Cells[1].Controls[0]).Text);
            decimal unitprice = Convert.ToDecimal(((TextBox)row.Cells[2].Controls[0]).Text);
            decimal totalamount = quantity * unitprice;

            DataTable dt = ViewState["dt"] as DataTable;
            dt.Rows[row.RowIndex]["ItemName"] = itemname;
            dt.Rows[row.RowIndex]["Quantity"] = quantity;
            dt.Rows[row.RowIndex]["UnitPrice"] = unitprice;
            dt.Rows[row.RowIndex]["TotalAmount"] = totalamount;
            ViewState["dt"] = dt;
            GridView1.EditIndex = -1;
            BindGrid();
        }

        //cancel occurs after clicking edit button

        protected void OnCancel(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            BindGrid();
        }



        //gridview delete code

        protected void OnDelete(object sender, EventArgs e)
        {
            GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
            DataTable dt = ViewState["dt"] as DataTable;
            dt.Rows.RemoveAt(row.RowIndex);
            ViewState["dt"] = dt;
            BindGrid();
        }

        //insert code

        protected void btnAddtolist_Click(object sender, EventArgs e)
        {

            DataTable dt = ViewState["dt"] as DataTable;
            decimal total = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtUnit_price.Text);
            dt.Rows.Add(txtItem_name.Text, txtQuantity.Text, txtUnit_price.Text, total);
            txtTotal_amount.Text = total.ToString();
            ViewState["dt"] = dt;
            BindGrid();
            txtItem_name.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtUnit_price.Text = string.Empty;
            txtTotal_amount.Text = string.Empty;

        }

        // save part

        protected void btnSave_Click(object sender, EventArgs e)
        {


            con.Open();
            SqlCommand cd = new SqlCommand(@"INSERT INTO [dbo].[PurchaseInfo]
           ([PurchaseID]
           ,[PurchaseBy]
           ,[Date]
           ,[CompanyName])
     VALUES
           ('" + txtID.Text + "','" + dropdownPurchase_by.SelectedValue + "','" + txtDate.Text + "','" + dropdownCompany.SelectedValue + "')", con);
            cd.ExecuteNonQuery();
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                SqlCommand cmd = new SqlCommand(@"INSERT INTO [dbo].[PurchaseDetailsInfo]
           ([PurchaseID]
           ,[ItemName]
           ,[Quantity]
           ,[UnitPrice]
           ,[TotalAmount])
     VALUES
           ('" + txtID.Text + "','" + GridView1.Rows[i].Cells[0].Text + "','" + GridView1.Rows[i].Cells[1].Text + "','" + GridView1.Rows[i].Cells[2].Text + "','" + GridView1.Rows[i].Cells[3].Text + "')", con);
                cmd.ExecuteNonQuery();
            }
            Response.Write("Data has been inserted Successfully");
            clear();
            con.Close();
        }

        protected void totalCalculation(object sender, EventArgs e)
        {
            if (!IsNumeric(txtQuantity.Text))
            {
                lblErrorMessage.Text = "Quantity is not valid";
                txtQuantity.Text = "";
                txtQuantity.Focus();
                return;
            }

            if (!IsNumeric(txtUnit_price.Text))
            {
                lblErrorMessage.Text = "Unit Price is not valid";
                txtUnit_price.Text = "";
                txtUnit_price.Focus();
                return;
            }

            decimal quantity = Convert.ToDecimal(txtQuantity.Text);
            decimal unitPrice = Convert.ToDecimal(txtUnit_price.Text);
            decimal total = quantity * unitPrice;
            txtTotal_amount.Text = total.ToString();
            lblErrorMessage.Text = "";

            //TextQuantity.Text = "";
            //TextUnitPrice.Text = "";

            //decimal total = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtUnit_price.Text);
            //txtTotal_amount.Text = total.ToString();

            //TextQuantity.Text = "";
            //TextUnitPrice.Text = "";
        }

        private bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        //clear part

        protected void clear()
        {
            txtSearch.Text = string.Empty;
            txtID.Text = string.Empty;
            dropdownPurchase_by.SelectedItem.Text = string.Empty;
            txtDate.Text = string.Empty;
            dropdownCompany.SelectedItem.Text = string.Empty;
            txtItem_name.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtUnit_price.Text = string.Empty;
            txtTotal_amount.Text = string.Empty;

            //validID.Text = string.Empty;
            //validDate.Text = string.Empty;
            //validItem_name.Text = string.Empty;
            //validQuantity.Text = string.Empty;
            //validUnit_price.Text = string.Empty;


            DataTable dt = ViewState["dt"] as DataTable;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows.RemoveAt(i);
                i--;


            }
            GridView1.DataSource = ViewState["dt"] as DataTable;
            GridView1.DataBind();

        }

        // refresh part

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

            clear();

        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SearchData();
        }

        protected void SearchData()
        {
            using (con)
            {
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.CommandText = @"SELECT * FROM PurchaseInfo WHERE PurchaseID = '" + txtSearch.Text.Trim() + "'";
                SqlCmd.Connection = con;
                con.Open();
                SqlCmd.Parameters.AddWithValue("@PurchaseID", txtSearch.Text.Trim());
                DataTable dtbl = new DataTable();
                SqlDataAdapter sqlDa = new SqlDataAdapter(SqlCmd);
                sqlDa.Fill(dtbl);
                txtID.Text = dtbl.Rows[0]["PurchaseID"].ToString();
                dropdownPurchase_by.SelectedItem.Text = dtbl.Rows[0]["PurchaseBy"].ToString();
                txtDate.Text = dtbl.Rows[0]["Date"].ToString();

                dropdownCompany.SelectedItem.Text = dtbl.Rows[0]["CompanyName"].ToString();
                //Second table ItemName,Quantity,UnitPrice,TotalAmount

                SqlDataAdapter SqlTable = new SqlDataAdapter(@"SELECT * FROM PurchaseDetailsInfo WHERE PurchaseID = '" + txtSearch.Text.Trim() + "'", con);

                SqlTable.SelectCommand.Parameters.AddWithValue("@PurchaseID", txtSearch.Text.Trim());
                DataTable dt = new DataTable();
                SqlTable.Fill(dt);
                GridView1.DataSource = dt;
                GridView1.DataBind();


            }

        }
    }
}
